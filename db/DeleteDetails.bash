#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

# 1 - ID of record to delete
ID="$1"

SQL="DELETE FROM Details WHERE id=${ID}"

if $QUERY "$SQL" ; then
    WriteLog "Details Successfully Deleted..."
    exit 0
fi 

WriteLog "Update Details Deleted..."
exit 1
