#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

# 1 - ID of the details to get
ID="$1"
SQL="SELECT * From Details WHERE id = ${ID};"

if $QUERY "$SQL" ; then
    WriteLog "Details Successfully Obtained..."
    exit 0
fi

WriteLog "Get Details Failed..."
exit 1
