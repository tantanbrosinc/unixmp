#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

NAME="$1"
CASE="$2"
PROCESSOR="$3"
GPU="$4"
RAM="$5"
STORAGE="$6"
COOLING="$7"
POWERSUPPLY="$8"
DATEORDERED="$9"

SQL="INSERT INTO Details(name, cpu_case, processor, gpu, ram, storage, cooling_type, power_supply, date_ordered)
     VALUES ('${NAME}', '${CASE}', '${PROCESSOR}', '${GPU}', '${RAM}', '${STORAGE}', '${COOLING}', '${POWERSUPPLY}', '${DATEORDERED}');"

# Date now
if $QUERY "$SQL"; then
    WriteLog "Contact Successfully Added..."
    exit 0
fi

WriteLog "Add Contact Failed..."
exit 1
