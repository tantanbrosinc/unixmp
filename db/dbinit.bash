#!/bin/bash

DBCONFIG=$(find ./* -iname "db.conf")
source $DBCONFIG

RootQuery()
{
    # # First Argument is the sql statement
    SQL="$1"
    mysql -uroot -e"$SQL"
}

# To delete a user
DROPUSER="DROP USER IF EXISTS ${DBUSER}@${DBHOSTNAME};"

# To delete a DATABASE
DROPDB="DROP DATABASE IF EXISTS ${DBNAME};"

# Show all databases
SHOWDB="SHOW DATABASES;"

# Show all users
SHOWUSERS="SELECT User FROM mysql.user;"

# Create a user
CREATEUSER="CREATE USER IF NOT EXISTS ${DBUSER}@${DBHOSTNAME} IDENTIFIED BY '${DBPASS}';"

# Create the Database
CREATEDB="CREATE DATABASE IF NOT EXISTS ${DBNAME}"

# Grant Privileges to User for Database
GRANTUSER="GRANT ALL PRIVILEGES ON ${DBNAME}.* TO '${DBUSER}'@'${DBHOSTNAME}' IDENTIFIED BY '${DBPASS}';"

# Revoke all privileges
REVOKEUSER="REVOKE ALL PRIVILEGES, GRANT OPTION FROM ${DBUSER}@${DBHOSTNAME};"

# Use Database
USEDB="USE ${DBNAME};"

# Create the Details table
CREATETABLE="\
USE ${DBNAME};\
create table Details
(
	id int auto_increment, \
	name varchar(256) not null, \
	cpu_case varchar(256) not null, \
	processor varchar(256) not null, \
	gpu varchar(256) not null, \
	ram varchar(256) not null, \
	storage varchar(256) not null, \
	cooling_type varchar(256) not null, \
	power_supply varchar(256) not null, \
	date_ordered DATE not null, \
	constraint Details_pk \
		primary key (id) \
);"

# Show Tables
SHOWTABLES="SHOW TABLES;"

# Show Columns
SHOWCOLUMNS="SHOW COLUMNS FROM TABLENAME;"

if ! RootQuery "$CREATEUSER"; then
    echo "Create User Failed..."
    exit 1
fi
echo "Create User Success..."

if ! RootQuery "$CREATEDB"; then
    echo "Create Database Failed..."
    exit 1
fi
echo "Create Database Success..."

if ! RootQuery "$GRANTUSER"; then
    echo "Grant Privileges to User Failed..."
    exit 2
fi
echo "Grant Privileges to User Success..."

if ! RootQuery "$CREATETABLE"; then
    echo "Create Table Failed..."
    exit 3
fi
echo "Create Table Success..."

exit 0
