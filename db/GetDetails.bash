#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

SQL="SELECT * From Details;"

if $QUERY "$SQL" ; then
    WriteLog "Details Successfully Obtained..."
    exit 0
fi

WriteLog "Get Details Failed..."
exit 1
