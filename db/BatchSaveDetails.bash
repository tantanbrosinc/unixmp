#!/bin/bash

shopt -s expand_aliases
alias Log=$(find ../* -iname "Logger.bash")
QUERY=$(find ./* -iname "query.bash")

DATA="$1"

SQL="INSERT INTO Details(name, cpu_case, processor, gpu, ram, storage, cooling_type, power_supply, date_ordered)
     VALUES ${DATA};"

# Date now
if $QUERY "$SQL"; then
    Log "Contact Successfully Added..."
    exit 0
fi

Log "Add Contact Failed..."
exit 1
