#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

# 1 - field to search
# 2 - what to search

SFIELD="$1"
SVALUE="$2"

SQL="SELECT * FROM Details WHERE ${1} LIKE \"%${2}%\";"

if $QUERY "$SQL" ; then
    WriteLog "Details Successfully Searched..."
    exit 0
fi

WriteLog "Search Details Failed..."
exit 1
