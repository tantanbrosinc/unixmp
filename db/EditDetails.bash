#!/bin/bash

shopt -s expand_aliases
LOGGER=$(find ../* -iname "Logger.bash")
source "$LOGGER"
QUERY=$(find ./* -iname "query.bash")

# 1 - ID of record to update
# 2 - field to update
# 3 - new value
ID="$1"
FIELD="$2"
VALUE="$3"

SQL="UPDATE Details SET ${FIELD}=\"${VALUE}\" WHERE id=${ID}"

if $QUERY "$SQL" ; then
    WriteLog "Details Successfully Updated..."
    exit 0
fi 

WriteLog "Update Details Failed..."
exit 1
