#!/bin/bash
WriteLog()
{
    MESSAGE="$1"
    DATETIME=$(date +"%D %T")
    LOGSTRING="[$DATETIME] $MESSAGE"
    echo $LOGSTRING >> $LOGFILE
}