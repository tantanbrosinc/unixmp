#!/bin/bash
export Details_NAME='name'
export Details_CASE='cpu_case'
export Details_PROCESSOR='processor'
export Details_GPU='gpu'
export Details_RAM='ram'
export Details_STORAGE='storage'
export Details_COOLING='cooling_type'
export Details_POWERSUPPLY='power_supply'
export Details_DATEORDERED='date_ordered'
export Details_ALL=( 
    $Details_NAME 
    $Details_CASE 
    $Details_PROCESSOR
    $Details_GPU
    $Details_RAM
    $Details_STORAGE
    $Details_COOLING
    $Details_POWERSUPPLY
    $Details_DATEORDERED
)
export Details_NUM=${#Details_ALL[*]}
