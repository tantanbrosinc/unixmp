#!/bin/bash

PrintTable()
{
    local -r delimiter="${1}"
    local -r data="$(RemoveEmptyLines "${2}")"

    if [[ "${delimiter}" != '' && "$(IsEmptyString "${data}")" = 'false' ]]
    then
        local -r numberOfLines="$(wc -l <<< "${data}")"

        if [[ "${numberOfLines}" -gt '0' ]]
        then
            local table=''
            local i=1

            for ((i = 1; i <= "${numberOfLines}"; i = i + 1))
            do
                local line=''
                line="$(sed "${i}q;d" <<< "${data}")"

                local numberOfColumns='0'
                numberOfColumns="$(awk -F "${delimiter}" '{print NF}' <<< "${line}")"

                # Add Line Delimiter

                if [[ "${i}" -eq '1' ]]
                then
                    table="${table}$(printf '%s#+' "$(RepeatString '#+' "${numberOfColumns}")")"
                fi

                # Add Header Or Body

                table="${table}\n"

                local j=1

                for ((j = 1; j <= "${numberOfColumns}"; j = j + 1))
                do
                    table="${table}$(printf '#| %s' "$(cut -d "${delimiter}" -f "${j}" <<< "${line}")")"
                done

                table="${table}#|\n"

                # Add Line Delimiter

                if [[ "${i}" -eq '1' ]] || [[ "${numberOfLines}" -gt '1' && "${i}" -eq "${numberOfLines}" ]]
                then
                    table="${table}$(printf '%s#+' "$(RepeatString '#+' "${numberOfColumns}")")"
                fi
            done

            if [[ "$(IsEmptyString "${table}")" = 'false' ]]
            then
                echo -e "${table}" | column -s '#' -t | awk '/^\+/{gsub(" ", "-", $0)}1'
            fi
        fi
    fi
}

RemoveEmptyLines()
{
    local -r content="${1}"

    echo -e "${content}" | sed '/^\s*$/d'
}

RepeatString()
{
    local -r string="${1}"
    local -r numberToRepeat="${2}"

    if [[ "${string}" != '' && "${numberToRepeat}" =~ ^[1-9][0-9]*$ ]]
    then
        local -r result="$(printf "%${numberToRepeat}s")"
        echo -e "${result// /${string}}"
    fi
}

IsEmptyString()
{
    local -r string="${1}"

    if [[ "$(TrimString "${string}")" = '' ]]
    then
        echo 'true' && return 0
    fi

    echo 'false' && return 1
}

TrimString()
{
    local -r string="${1}"

    sed 's,^[[:blank:]]*,,' <<< "${string}" | sed 's,[[:blank:]]*$,,'
}

SortSwap()
{
    # swap content string
    STMP="${STARR[$i]}"
    STARR[$i]="${STARR[$j]}"
    STARR[$j]="$STMP"

    # swap line number
    LNTMP="${LNARR[$i]}"
    LNARR[$i]="${LNARR[$j]}"
    LNARR[$j]="$LNTMP"
}

PrepareForSort()
{
    STARR=()
    LNARR=()
    for i in $(seq 0 $(($NLINE-1))); do
        STARR[$i]=$(echo "$VALUES" | tr "\t" ":" | cut -f"$FIELDNUM" -d":" | head -$(($i+1)) | tail -1)
        (( LNARR[$i]=$i+1 ))

        # echo "${LNARR[$i]}: ${STARR[$i]}"
    done
}

PerformSort()
{
    for i in $(seq 0 $(($NLINE-1))); do
        for j in $(seq $(($i+1)) $NLINE); do
            if [[ "${STARR[$i]}" > "${STARR[$j]}" ]]; then
                SortSwap
            fi
        done
    done
}

ShowSortRecordMenu()
{
    clear
    echo "################################################################"
    echo "Sort Records"
    echo "################################################################"
    echo "1) Id"
    echo "2) Name"
    echo "3) CPU Case"
    echo "4) Processor"
    echo "5) GPU"
    echo "6) RAM"
    echo "7) Storage"
    echo "8) Cooling Type"
    echo "9) Power Supply"
    echo "10) Date Ordered"
    echo -n "What field would you like to be sorted? "
    read FIELDNUM

    INDEX=$(( $FIELDNUM - 1 ))
    if [ $INDEX -ge 0 -a $INDEX -lt 10 ]; then
        
        DETAILS="$(GetDetails)"
        NUMLINES=$(echo "$DETAILS" | wc -l) # header + value lines
        NLINE=$(( $NUMLINES-1 )) # value lines
        HEADERS=$(echo -e "$DETAILS" | head -1)
        VALUES=$(echo -e "$DETAILS" | tail -n "$NLINE" | tr "\t" ":")

        PrepareForSort
        PerformSort

        TEMPFILE="var/tmp/sorted.txt"
        echo "$HEADERS" | tr "\t" ":" > "$TEMPFILE"
        for i in $(seq 1 $(($NLINE))); do
            echo "$VALUES" | sed "${LNARR[$i]}q;d" >> "$TEMPFILE"
        done

        PrintTable ":" "$(cat $TEMPFILE)"

    else
        echo "Invalid Field Selected"
        WriteLog "Search: Invalid Field Selected"
    fi

    echo -e "\n\nPress any key to return to main menu..."
    read -n1
}