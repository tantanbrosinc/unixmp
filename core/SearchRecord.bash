#!/bin/bash

ReadSearchRecordMenuAction()
{
    echo -n "What field would you like to search for? "
    read FIELDNUM
    echo ""

    if [ $FIELDNUM -eq 10 ]; then
        return;
    fi

    WriteLog "User entered $FIELDNUM"

    INDEX=$(( $FIELDNUM - 1 ))
    if [ $INDEX -ge 0 -a $INDEX -lt $Details_NUM ]; then
        FIELD=${Details_ALL[INDEX]}
        echo -n "Enter value to search: "
        read VALUE

        WriteLog "Search: Field = $FIELD, Value = $VALUE"
        echo "################################################################"
        echo "Search Results"
        echo "################################################################"
        SearchDetails "$FIELD" "$VALUE"

        echo -e "\n\nPress any key to return to viewing records..."
        read -n1
        ShowViewRecordWindow
    else
        echo "Invalid Field Selected"
        WriteLog "Search: Invalid Field Selected"

        echo -e "\n\nPress any key to continue searching..."
        read -n1
        ShowSearchRecordMenu
    fi
}

ShowSearchRecordMenu()
{
    clear

    echo "################################################################"
    echo "Search Records"
    echo "################################################################"
    echo "1) Name"
    echo "2) CPU Case"
    echo "3) Processor"
    echo "4) GPU"
    echo "5) RAM"
    echo "6) Storage"
    echo "7) Cooling Type"
    echo "8) Power Supply"
    echo "9) Date Ordered"
    echo "10) Exit to Menu"

    ReadSearchRecordMenuAction
}