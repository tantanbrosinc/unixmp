#!/bin/bash

ShowAddRecordMenu()
{
    echo "Please fill up the necessary information:"

    echo -n "Enter Name of Customer: "
    read NAME

    echo -n "Enter CPU Case: "
    read CASE

    echo -n "Enter Processor: "
    read PROCESSOR

    echo -n "Enter GPU: "
    read GPU

    echo -n "Enter RAM: "
    read RAM

    echo -n "Enter Storage: "
    read STORAGE

    echo -n "Select Cooling Type (Cooling Fan or Watercool): "
    read COOLING

    echo -n "Enter Power Supply: "
    read POWERSUPPLY

    # Date now
    DATEORDERED=$(date +"%F")

    echo -n "Confirm Add Record [Y/n]: "
    read CONFIRM

    case $CONFIRM in
        [Yy]) 
             SaveDetails \
                "${NAME}" \
                "${CASE}" \
                "${PROCESSOR}" \
                "${GPU}" \
                "${RAM}" \
                "${STORAGE}" \
                "${COOLING}" \
                "${POWERSUPPLY}" \
                "${DATEORDERED}" \

            echo ""
            echo "The following record has been saved"
            # Display newly saved record
            GetLatestDetails
        ;;
    esac

    echo -e "\nPress any key to go back to the menu..."
    read -n1
}

