#!/bin/bash

Quit() 
{
    echo "Goodbye..."
    APPSTATE=$APPSTATE_STOP
}

ReadMenuAction()
{
    echo -n "What would you like to do? "
    read ACTION
    echo ""

    WriteLog "User entered $ACTION"

    case $ACTION in 
        [1]) ShowAddRecordMenu ;;
        [2]) ShowViewRecordWindow ;;
        [3]) ShowBatchAddRecordMenu ;;
        [4]) Quit ;;
    esac
}

ShowMenu()
{
    WriteLog "Show Menu"
    echo "################################################################"
    echo "Welcome to Pixie Express Computer Parts Shop"
    echo "################################################################"
    echo "1) Add Record"
    echo "2) View Records"
    echo "3) Load Records"
    echo "4) Exit"

    ReadMenuAction
}