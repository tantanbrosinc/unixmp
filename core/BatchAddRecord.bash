#!/bin/bash


ShowBatchAddRecordMenu()
{
    clear
    echo "################################################################"
    echo "Load Batch Records"
    echo "################################################################"

    echo -n "Enter the location of the file you want to save: "
    read BATCHFILE

    FULLPATH=$(readlink -f "$BATCHFILE")

    echo "Loading Batch File: $FULLPATH"
    if [[ -f "$FULLPATH" ]]; then
        # Trim trailing newlines w/ awk
        # Enclose each row with '(),' w. sed
        # remove trailing comma
        DATA=$(cat $FULLPATH | awk /./ | sed 's/^/(/g' | sed 's/$/),/g' | sed '$ s/.$//')
        BATCHSAVED=$(BatchSaveDetails "$DATA")

        if [ -z "$BATCHSAVED" ]; then
            SUCCESSMSG="Batch file successfully loaded"
            echo $SUCCESSMSG
            WriteLog $SUCCESSMSG

            echo -e "\nPress any key to view the loaded records..."
            read -n1        
            ShowViewRecordWindow
        else
            ERRMSG="Error in loading the batch file"
            echo $ERRMSG
            WriteLog $ERRMSG
        fi
    else
        ERRMSG="Error: File not found: $FULLPATH"
        echo $ERRMSG
        WriteLog $ERRMSG
    fi

    echo -e "\nPress any key to go back to the menu..."
    read -n1
}