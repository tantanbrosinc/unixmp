#!/bin/bash

ShowEditRecordHeader()
{
    echo "################################################################"
    echo "Edit Record"
    echo "################################################################"
}

ReadEditRecordMenuAction()
{
    echo -n "What field would you like to edit? "
    read FIELDNUM

    INDEX=$(( $FIELDNUM - 1 ))
    if [ $INDEX -ge 0 -a $INDEX -lt $Details_NUM ]; then
        FIELD=${Details_ALL[INDEX]}
        echo -n "Enter new value: "
        read VALUE

        WriteLog "Edit: ID = $ID, Field = $FIELD, Value = $VALUE"
        EditDetails "$ID" "$FIELD" "$VALUE"
    else
        echo "Invalid Field Selected"
        WriteLog "Search: Invalid Field Selected"
    fi

    clear
    ShowEditRecordHeader
    echo -e "\nRecord has been successfully updated..."
    GetDetailsById "$ID"

}

ShowEditRecordMenu()
{
    ShowEditRecordHeader
    echo -n "Enter the ID of the record you want to edit: "
    read ID
    echo ""

    DETAILS=$(GetDetailsById "$ID")
    if [ -z "$DETAILS" ]; then
        echo "Entered ID was not found"
    else
        echo "1) Name"
        echo "2) CPU Case"
        echo "3) Processor"
        echo "4) GPU"
        echo "5) RAM"
        echo "6) Storage"
        echo "7) Cooling Type"
        echo "8) Power Supply"
        echo "9) Date Ordered"

        ReadEditRecordMenuAction
    fi

    echo -e "\n\nPress any key to return to viewing records..."
    read -n1
    ShowViewRecordWindow
}