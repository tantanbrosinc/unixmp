#!/bin/bash

ShowDeleteRecordHeader()
{
    echo "################################################################"
    echo "Delete Record"
    echo "################################################################"
}

ReadDeleteRecordMenuAction()
{
    DELETED=$(DeleteDetails "$ID")
    if [ -z "$DELETED" ]; then
        DELETEMSG="Record with ID=$ID has been successfully deleted"
        echo -n "$DELETEMSG"
        WriteLog "$DELETEMSG"
    else 
        DELETEMSG="There was a problem in deleting the record"
        echo -n "$DELETEMSG"
        WriteLog "$DELETEMSG"
    fi
}

ShowDeleteRecordMenu()
{
    ShowDeleteRecordHeader

    echo -n "Enter the ID of the record you want to delete: "
    read ID
    echo ""

    DETAILS=$(GetDetailsById "$ID")
    if [ -z "$DETAILS" ]; then
        echo "Entered ID was not found"
    else
        ReadDeleteRecordMenuAction
    fi

    echo -e "\n\nPress any key to return to viewing records..."
    read -n1
    ShowViewRecordWindow
}