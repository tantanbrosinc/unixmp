#!/bin/bash

ReadViewRecordMenuAction()
{
    echo -n "What would you like to do? "
    read ACTION
    echo ""

    WriteLog "User entered $ACTION"

    case $ACTION in 
        [1]) ShowSearchRecordMenu ;;
        [2]) ShowSortRecordMenu ;;
        [3]) ShowEditRecordMenu ;;
        [4]) ShowDeleteRecordMenu ;;
        [5]) ;;
    esac
}

ShowViewRecordHeader()
{
    echo "################################################################"
    echo "Viewing All Records"
    echo "################################################################"
}

ShowViewRecordMenu()
{
    WriteLog "Show View Record Menu"
    echo ""
    echo "1) Search for Records"
    echo "2) Sort the Records"
    echo "3) Edit a Record"
    echo "4) Delete a Record"
    echo "5) Exit to main menu"
}

ShowViewRecordWindow()
{
    clear
    ShowViewRecordHeader
    GetDetails
    ShowViewRecordMenu
    ReadViewRecordMenuAction
}