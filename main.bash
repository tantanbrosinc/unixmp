#!/bin/bash

source "startup.bash"

StartApplication()
{
    WriteLog "Application Started..."
    until [ $APPSTATE -eq $APPSTATE_STOP ]; do
        clear
		ShowMenu
        echo ""
	done
    WriteLog "Application Stopped..."
}

StartApplication

exit $EXITSTATUS

