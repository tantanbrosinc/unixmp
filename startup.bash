#!/bin/bash
# set up BASE_DIR program path
PATH=$PATH:$PWD

# set up inner dirs
DIRS=( ls -d */ )

# Add the DIRS to the PATH
for DIR in ${DIRS[*]}; do
    PATH=$PATH:"${PWD}/${DIR%/}"
done

# for aliases a script need this
shopt -s expand_aliases

# set up aliases

# make all bash files executable
find ./* -iname "*.bash" -exec chmod +x {} +

# set up directory variables
BASE_DIR="$PWD"
CORE_DIR="${BASE_DIR}/core"
DB_DIR="${BASE_DIR}/db"
LOGS_DIR="${BASE_DIR}/var/logs"

alias SaveDetails="bash '${DB_DIR}/SaveDetails.bash'"
alias GetDetails="bash '${DB_DIR}/GetDetails.bash'"
alias GetLatestDetails="bash '${DB_DIR}/GetLatestDetails.bash'"
alias SearchDetails="bash '${DB_DIR}/SearchDetails.bash'"
alias EditDetails="bash '${DB_DIR}/EditDetails.bash'"
alias GetDetailsById="bash '${DB_DIR}/GetDetailsById.bash'"
alias DeleteDetails="bash '${DB_DIR}/DeleteDetails.bash'"
alias BatchSaveDetails="bash '${DB_DIR}/BatchSaveDetails.bash'"



# source all scripts in the core directory
SCRIPTS=( $(find ${CORE_DIR} -iname "*.bash") )
for SCRIPT in ${SCRIPTS[*]}; do
    source "$SCRIPT"
done


# set up exported variables
export LOGFILE="${LOGS_DIR}/logs.txt"
export APPSTATE=$APPSTATE_MENU
export EXITSTATUS=$EXITSTATUS_NORMAL

# Replace contents of log file every program start
touch "$LOGFILE"
echo -n > "$LOGFILE"
